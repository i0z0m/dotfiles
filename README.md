# set
```console
git clone https://gitlab.com/i0z0m/dotfiles.git ~/
```
or
```console
bash -c "$(curl -L gitlab.com/i0z0m/dotfiles/-/raw/master/get.sh)"
```

```console
cd ~/dotfiles
make deploy
./set.sh
```
